--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: user_game; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_game (
    id bigint NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(255) NOT NULL,
    avatar character varying(255) NOT NULL
);


ALTER TABLE public.user_game OWNER TO postgres;

--
-- Name: user_game_biodata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_game_biodata (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    fullname character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    tel character varying(15) NOT NULL,
    birthday date NOT NULL,
    region character varying(100) NOT NULL
);


ALTER TABLE public.user_game_biodata OWNER TO postgres;

--
-- Name: user_game_biodata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_biodata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_biodata_id_seq OWNER TO postgres;

--
-- Name: user_game_biodata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_biodata_id_seq OWNED BY public.user_game_biodata.id;


--
-- Name: user_game_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_game_history (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    spend_time integer DEFAULT 0 NOT NULL,
    result character varying(10),
    score integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_game_history OWNER TO postgres;

--
-- Name: user_game_history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_history_id_seq OWNER TO postgres;

--
-- Name: user_game_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_history_id_seq OWNED BY public.user_game_history.id;


--
-- Name: user_game_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_game_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_game_id_seq OWNER TO postgres;

--
-- Name: user_game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_game_id_seq OWNED BY public.user_game.id;


--
-- Name: user_game id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game ALTER COLUMN id SET DEFAULT nextval('public.user_game_id_seq'::regclass);


--
-- Name: user_game_biodata id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_biodata ALTER COLUMN id SET DEFAULT nextval('public.user_game_biodata_id_seq'::regclass);


--
-- Name: user_game_history id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history ALTER COLUMN id SET DEFAULT nextval('public.user_game_history_id_seq'::regclass);


--
-- Data for Name: user_game; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_game (id, username, password, avatar) FROM stdin;
1	sekarmk03	1234	cat.png
2	azarn	1234	owl.png
3	horanpatronus	4321	cow.png
4	mdhstama	4321	eel.png
5	hrahmad	6789	pig.png
6	zektv	6789	dog.png
7	ikim	9876	fox.png
8	anip	9876	bat.png
\.


--
-- Data for Name: user_game_biodata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_game_biodata (id, user_id, fullname, email, tel, birthday, region) FROM stdin;
3	3	Rahma Maulida	rahma_m@gmail.com	089897765567	2002-03-05	Indonesia
4	4	Aditya Hasta Pratama	mdhstama@upi.edu	089123654789	2002-09-11	Indonesia
5	5	Hilman Ahmad Rusydi	hr_ahmad@upi.edu	089135792468	2001-06-30	Indonesia
7	7	Driski Maulana	driskimaulana@upi.edu	081234567890	2002-08-15	Indonesia
8	8	Hanifah Alhumaira	hahhanifah@gmail.com	088987654321	2002-11-10	Indonesia
1	1	Sekar Madu Kusumawardani	sekarmadu99@gmail.com	089691798633	2002-07-03	Indonesia
2	2	Muhammad Azar Nuzy	azarnuzy@gmail.com	082246449106	2001-11-06	Indonesia
6	6	Zakaria Saputra	zakariasptr@gmail.com	089111222333	2002-06-06	Indonesia
\.


--
-- Data for Name: user_game_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_game_history (id, user_id, spend_time, result, score) FROM stdin;
1	2	180	win	3259
2	2	79	draw	1829
3	1	233	draw	4335
4	1	201	lose	3702
5	1	304	win	4809
6	2	207	win	3711
7	3	340	win	4542
8	4	340	lose	3231
9	4	421	draw	6566
10	5	421	draw	6566
11	6	870	win	1027
12	6	563	win	929
13	5	309	win	556
14	7	564	lose	376
15	2	779	win	1084
16	1	300	win	506
17	7	322	win	682
\.


--
-- Name: user_game_biodata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_biodata_id_seq', 8, true);


--
-- Name: user_game_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_history_id_seq', 17, true);


--
-- Name: user_game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_game_id_seq', 8, true);


--
-- Name: user_game_biodata user_game_biodata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_biodata
    ADD CONSTRAINT user_game_biodata_pkey PRIMARY KEY (id);


--
-- Name: user_game_history user_game_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history
    ADD CONSTRAINT user_game_history_pkey PRIMARY KEY (id);


--
-- Name: user_game user_game_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game
    ADD CONSTRAINT user_game_pkey PRIMARY KEY (id);


--
-- Name: user_game_biodata fk_user_game; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_biodata
    ADD CONSTRAINT fk_user_game FOREIGN KEY (user_id) REFERENCES public.user_game(id);


--
-- Name: user_game_history fk_user_game; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_game_history
    ADD CONSTRAINT fk_user_game FOREIGN KEY (user_id) REFERENCES public.user_game(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

